require 'yaml'
require 'version_sorter'
require_relative 'releases'

class ReleasePage
  def releases_count
    ::ReleaseList.new.count
  end

  def releases_data
    @releases_data ||= begin
      data = YAML.safe_load(File.read(releases_file))

      VersionSorter
        .sort(data) { |data| data['version'] }
    end
  end

  def current_version
    releases_data
      .find { |release| Date.parse(release['date']) < Date.today }
      .fetch('version')
  end

  private

  def releases_file
    File.join(File.dirname(__FILE__), '../data/releases.yml')
  end
end
